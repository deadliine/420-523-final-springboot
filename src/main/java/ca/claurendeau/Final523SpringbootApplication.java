package ca.claurendeau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Final523SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Final523SpringbootApplication.class, args);
	}
}
